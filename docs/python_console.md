# The Python console

**API Note:** Any QGIS APIs referenced will link out to the [Python API Docs](https://qgis.org/pyqgis/master).

### Before Starting

Open the `project.qgz`

### Exploring the console

The Python Console and Python script editor is a great way to explore the 
QGIS api in order to try out the logic of your code before adding it to you scripts.

We won't spend long in here, just enough to try out some basic QGIS APIs and get used to the UI.

#### Exercise: Using the console

Open the console using `Plugins -> Python Console`

### Running Commands

Inside the console we can run any commands we want to try. This is a full interactive Python console so any
python code will work here.

#### Exercise: First Python commands

Try something basic like the following and hit enter

```
print("foss4g rocks")
```

The output is:

```
foss4g rocks
```

#### Exercise: First PyQGIS commands

Lets try some QGIS commands. Try each one and check the results


```
print(Qgis.QGIS_RELEASE_NAME)
```

```
print(iface.activeLayer().name())
```

```
print(iface.activeLayer().featureCount())
```

#### Intro the `iface` object

QGIS exposes a object called `iface` ([QgisInterface](https://qgis.org/pyqgis/master/gui/other/QgisInterface.html?#module-QgisInterface)). 
This object exposes methods that you can use to make some more complex operations easier. It's a wrapper around some of
the lower level object.  One example is `iface.newProject()`.

#### Exercise: New project from PyQGIS

Run the following command to create a new project

```
iface.newProject()
```

#### Exercise: Access the main canvas

We can access the main canvas and it's scale function from here.

```
iface.mapCavnas().scale()
```

Lets move on the the script editor

# The script editor

### Before Starting

Open the `project.qgz`

From inside the console we can open the script editor. This editor can be used to write more complex scripts where the console
is normally only used for smaller one off logic.

#### Exercise: Opening the editor

Open the editor from inside the Python Console using the following button and the script editor will open on the left. 

![Function Editor](img/editor.png)

For a quick test we can write the a simple one line command and run the script

#### Exercise: Running the first commands

```
print(iface.activeLayer().name())
```

![Function Editor](img/editor_output.png)

##### Tip: Run selected text 

You can run just the selected text as well with `Ctrl+E` if you have a more complex script and need to check 
just parts of it.


#### Lower case all layer names

For this example we will create a new file using the plus button and write a script to lower case all the layer 
names in the project.

#### Exercise: Import QGIS modules

Although QGIS has auto imported all the QGIS APIs into the console it's good practice to import them in your scripts to make
things clear

In the new script add the following at the top

```
from qgis.core import QgsProject
```

Now we are ready to loop over all the layers and rename them to lower case.  Copy the following into the script editor 
and use the green edit button to run the script

#### Exercise: Upper case all layer names

```
for layer in QgsProject.instance().mapLayers().values():
     print(layer.name())
     layer.setName(layer.name().upper())
     print("New name", layer.name())
```

The full script should look like this:

```
from qgis.core import QgsProject
for layer in QgsProject.instance().mapLayers().values():
     print(layer.name())
     layer.setName(layer.name().upper())
     print("New name", layer.name())
```

The output in the console should be

![Function Editor](img/layer_name_out.png)

and the layers panel with the new names

![Function Editor](img/legend_out.png)


